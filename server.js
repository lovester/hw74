const express = require('express');
const messages = require('./messages');
const messagesDb = require('./messagesDb');
const app = express();
messagesDb.init();

app.use(express.json());

const port = 8000;

app.use('/messages', messages);

app.listen(port, () => {
    console.log(`Server started on ${port} port`);
});
