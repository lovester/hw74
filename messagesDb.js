const fs = require('fs');
const path = "./messages";

let messages = [];

module.exports = {
    init() {
        fs.readdir(path, (err, files) => {
            messages = files.map(file => {
                const message = fs.readFileSync(path + '/' + file, 'utf8');
                return JSON.parse(message);
            });
        });
        return messages.slice(messages.length -5);
    },
    getItems() {
        return this.init();
    },
    addItem(item) {
        const date = new Date().toISOString();
        const message = {
            message: item,
            date: date
        };
        fs.writeFileSync(`./messages/${date}.txt`, JSON.stringify(message));
    }
};
