const express = require('express');
const messagesDb = require('./messagesDb');
const router = express.Router();


router.get('/', (req, res) => {
    const messages = messagesDb.getItems();
    console.log(messages);
    res.send(messages);
});

router.post('/', (req, res) => {
    messagesDb.addItem(req.body.message);
    res.send({message: 'OK'});
});

module.exports = router;